"""
URL configuration for blogprj project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from blog_app.views import (
    PostListCreateApiView,
    PostUpdateApiView,
    PostDeleteApiView,
    CommentViewSet
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('posts/', PostListCreateApiView.as_view()),
    path('posts/<int:pk>/', PostUpdateApiView.as_view()),
    path('posts/<int:pk>/delete/', PostDeleteApiView.as_view()),
    path('comments/', CommentViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('comments/<int:pk>/', CommentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
]
